package com.cfit.app.converters;

import com.cfit.app.commands.NewSessionCommand;
import com.cfit.app.models.Session;
import lombok.Synchronized;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class NewSessionCommandToSession implements Converter<NewSessionCommand, Session>{

    private Date parseDate(String date, String format) throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date);
    }

    @Synchronized
    @Nullable
    @Override
    public Session convert(NewSessionCommand source){

        if (source == null){
            return null;
        }

        final Session session = new Session();
        try {
            session.setDate(parseDate(source.getDatetime(), "yyyy-MM-dd hh:mm"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return session;
    }
}
