package com.cfit.app.converters;

import com.cfit.app.commands.WorkoutCommand;
import com.cfit.app.models.Workout;
import lombok.Synchronized;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

@Component
public class CommandToWorkout implements Converter<WorkoutCommand, Workout>{

    @Synchronized
    @Nullable
    @Override
    public Workout convert(WorkoutCommand source){
        if (source == null){
            return null;
        }

        final Workout workout = new Workout();

        workout.setName(source.getName());
        workout.setDescription(source.getDescription());
        workout.setSets(source.getSets());
        workout.setReps(source.getReps());
        workout.setKgs(source.getKgs());
        return workout;
    }
}
