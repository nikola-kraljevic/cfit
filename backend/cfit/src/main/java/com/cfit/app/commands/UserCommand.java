package com.cfit.app.commands;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Lob;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class UserCommand {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    @Lob
    private String diagnostics;
    private String subscription;
}
