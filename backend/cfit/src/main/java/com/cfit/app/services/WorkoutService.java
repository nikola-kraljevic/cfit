package com.cfit.app.services;

import com.cfit.app.models.Workout;

import java.util.List;


public interface WorkoutService {

    Workout findById(Long l);

    List<Workout> findWorkoutsByUserId(Long l);

    void deleteWorkout(Long l);
}
