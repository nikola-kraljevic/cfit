package com.cfit.app.services.Impl;

import com.cfit.app.commands.IdCommand;
import com.cfit.app.commands.NewSessionCommand;
import com.cfit.app.commands.SessionCommand;
import com.cfit.app.components.MailComponent;
import com.cfit.app.converters.NewSessionCommandToSession;
import com.cfit.app.converters.SessionToCommand;
import com.cfit.app.models.Session;
import com.cfit.app.models.User;
import com.cfit.app.repositories.SessionRepository;
import com.cfit.app.repositories.UserRepository;
import com.cfit.app.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class SessionServiceImpl implements SessionService {

    private final SessionRepository sessionRepository;
    private final UserRepository userRepository;
    private final SessionToCommand sessionToCommand;
    private final MailComponent mailComponent;
    private final NewSessionCommandToSession newSessionCommandToSession;

    @Autowired
    public SessionServiceImpl(SessionRepository sessionRepository, UserRepository userRepository, SessionToCommand sessionToCommand, MailComponent mailComponent, NewSessionCommandToSession newSessionCommandToSession) {
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
        this.sessionToCommand = sessionToCommand;
        this.mailComponent = mailComponent;
        this.newSessionCommandToSession = newSessionCommandToSession;
    }

    @Override
    public Session findById(Long l){
        Optional<Session> sessionOptional = sessionRepository.findById(l);
        if (!sessionOptional.isPresent()){
            throw new RuntimeException("Session with id" + l + "not found");
        }
        return sessionOptional.get();
    }

    @Override
    @Transactional
    public void deleteOld(){
        List<Session> list = sessionRepository.findAllByDateBefore(new Date());
        for(Session session : list){
            for (User user : session.getUsers()){
                user.setSessions(null);
            }
        }
        sessionRepository.deleteAll(list);
    }

    @Override
    @Transactional
    public void createNewAM(int daysAhead){
        int i=0;
        for (i=0;i<=1;i++){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, daysAhead);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, i+9);
            Session session = new Session();
            session.setDate(calendar.getTime());
            sessionRepository.save(session);
        }
    }

    @Override
    @Transactional
    public void createNewPM(int daysAhead){//MON and WED 19-21 2 users per session
        int i;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, daysAhead);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        for (i=0;i<=5;i++){
            if(calendar.get(calendar.DAY_OF_WEEK) == Calendar.FRIDAY && i == 0){
                i++;
            }
            calendar.set(Calendar.HOUR_OF_DAY, i+16);
            Session session = new Session();
            session.setDate(calendar.getTime());
            if(i>2 && (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)){
                session.setNumOfAllowedUsers(2);
            }
            sessionRepository.save(session);
        }
    }

    @Override
    @Transactional
    public void createNewSaturday(){
        int i=0;
        for (i=0;i<=2;i++){
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR,2);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, i+10);
            Session session = new Session();
            session.setDate(calendar.getTime());
            sessionRepository.save(session);
        }
    }

    @Override
    public List<Session> findAllAndSort(){
        List<Session> sessions = sessionRepository.findAll();

        Comparator<Session> comparator = Comparator.comparing(Session::getDate);
        Collections.sort(sessions, comparator);

        return sessions;
    }

    @Override
    public List<SessionCommand> findAvailable(Long userId){
        List<SessionCommand> mands = new ArrayList<>();
        User user = userRepository.findById(userId).get();

        for (Session sesh: findAllAndSort()) {
            if (sesh.getNumOfAllowedUsers() > 0 && !sesh.getUsers().contains(user)){
                SessionCommand sessionCommand = sessionToCommand.convert(sesh);
                sessionCommand.setDate(sesh.getDate());
                sessionCommand.setNumOfAllowedUsers(sesh.getNumOfAllowedUsers());
                mands.add(sessionCommand);
            }
        }

        Comparator<SessionCommand> comparator = Comparator.comparing(SessionCommand::getDate);
        Collections.sort(mands, comparator);

        return mands;
    }

    @Override
    public void incrementNumOfUsers(Long id){
        Session session = findById(id);
        session.setNumOfAllowedUsers(session.getNumOfAllowedUsers()+1);
        sessionRepository.save(session);
    }

    @Override
    public void decrementNumOfUsers(Long id){
        Session session = findById(id);
        session.setNumOfAllowedUsers(session.getNumOfAllowedUsers()-1);
        sessionRepository.save(session);
    }

    @Override
    @Transactional
    public List<Session> registerUser(Long sessionId, Long userId){
        Session session = findById(sessionId);
        User user = userRepository.findById(userId).get();
        List<Session> userSessions = user.getSessions();

        if(userSessions.size() < 2) {
            if(userSessions.size() > 0){
                Session registeredSession = userSessions.get(0);
                Date registeredDate = registeredSession.getDate();
                Calendar targetCal = Calendar.getInstance();
                Calendar registeredCal = Calendar.getInstance();
                targetCal.setTimeInMillis(session.getDate().getTime());
                registeredCal.setTimeInMillis(registeredDate.getTime());
                boolean sameDay = targetCal.get(Calendar.DAY_OF_YEAR) == registeredCal.get(Calendar.DAY_OF_YEAR) &&
                        targetCal.get(Calendar.YEAR) == registeredCal.get(Calendar.YEAR);
                if (!sameDay){
                    session.setNumOfAllowedUsers(session.getNumOfAllowedUsers()-1);
                    userSessions.add(session);
                    sessionRepository.save(session);
                    userRepository.save(user);
                }
            }else {
                session.setNumOfAllowedUsers(session.getNumOfAllowedUsers()-1);
                userSessions.add(session);
                sessionRepository.save(session);
                userRepository.save(user);
            }
        }

        return user.getSessions();
    }

    @Override
    public List<User> getUsersInSession(Long session_id){
        Session session = findById(session_id);
        return session.getUsers();
    }

    @Override
    @Transactional
    public void deleteSession(Long l) throws MessagingException {
        Session session = findById(l);

        for(User user: session.getUsers()){
            user.getSessions().remove(session);
            userRepository.save(user);
            mailComponent.deletedSessionMail(user.getEmail());
        }

        sessionRepository.delete(session);
    }

    @Override
    @Transactional
    public void newSession(NewSessionCommand command){
        Session session = newSessionCommandToSession.convert(command);
        sessionRepository.save(session);
    }
}
