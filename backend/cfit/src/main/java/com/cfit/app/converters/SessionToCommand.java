package com.cfit.app.converters;

import com.cfit.app.commands.SessionCommand;
import com.cfit.app.models.Session;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class SessionToCommand implements Converter<Session, SessionCommand>{

    @Synchronized
    @Nullable
    @Override
    public SessionCommand convert(Session source){
        if (source == null){
            return null;
        }

        final SessionCommand command = new SessionCommand();
        command.setId(source.getId());
        command.setDate(source.getDate());
        command.setNumOfAllowedUsers(source.getNumOfAllowedUsers());

        return command;
    }
}
