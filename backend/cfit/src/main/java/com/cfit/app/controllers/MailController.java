package com.cfit.app.controllers;

import com.cfit.app.commands.MailAllCommand;
import com.cfit.app.commands.MailAllResponseCommand;
import com.cfit.app.commands.RegistrationCommand;
import com.cfit.app.models.User;
import com.cfit.app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;
import java.util.List;

@RestController
public class MailController {

    private JavaMailSender sender;
    private final UserRepository userRepository;

    @Autowired
    public MailController(JavaMailSender sender, UserRepository userRepository) {
        this.sender = sender;
        this.userRepository = userRepository;
    }

    @ResponseBody
    @PostMapping("/email/warn")
    String home(@RequestBody RegistrationCommand command) throws Exception {
        sendEmail(command);
        return "Email Sent!";
    }

    @PostMapping("/email/everyone")
    public MailAllResponseCommand mailEveryone(@RequestBody MailAllCommand command) throws Exception{

        List<User> users = userRepository.findAll();
        MailAllResponseCommand responseCommand = new MailAllResponseCommand();
        responseCommand.setAll(users.size());
        responseCommand.setSent(0);
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        for (User user: users) {
            helper.setTo(user.getEmail());
            helper.setText(command.getText());
            helper.setSubject(command.getSubject());

            sender.send(message);
            responseCommand.setSent(responseCommand.getSent()+1);
            System.out.println("--- SENDING MAIL TO ------>" + responseCommand.getSent());
        }
        return responseCommand;
    }

    private void sendEmail(RegistrationCommand command) throws Exception{
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(command.getEmail());
        helper.setText(
                "Poštovani/a " + command.getFirstName() + ",\n" +
                "Danas ste izvršili prijavu za termin na koji niste došli. Ukoliko ne planirate doći molimo Vas da isti otkažete kako bi zauzeto mjesto bilo slobodno za druge prijave.\n" +
                "Hvala.");
        helper.setSubject("Termin");

        sender.send(message);
    }
}
