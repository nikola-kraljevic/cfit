package com.cfit.app.components;

import com.cfit.app.models.User;
import com.cfit.app.models.Workout;
import com.cfit.app.repositories.SessionRepository;
import com.cfit.app.repositories.UserRepository;
import com.cfit.app.repositories.WorkoutRepository;
import com.cfit.app.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@ConditionalOnProperty(
        value="prepopulate.database",
        havingValue = "true",
        matchIfMissing = true)
public class PrepopulateDatabase implements ApplicationListener<ContextRefreshedEvent> {

    private final UserRepository userRepository;
    private final WorkoutRepository workoutRepository;
    private final SessionRepository sessionRepository;
    private final SessionService sessionService;

    @Autowired
    public PrepopulateDatabase(UserRepository userRepository, WorkoutRepository workoutRepository, SessionRepository sessionRepository, SessionService sessionService) {
        this.userRepository = userRepository;
        this.workoutRepository = workoutRepository;
        this.sessionRepository = sessionRepository;
        this.sessionService = sessionService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event){
        prepopulate();
    }

    private void prepopulate(){
        int i = 0;
        int j = 0;
        String workouts[]= {"squat","deadlift","benchpress","shoulderpress"};
        for (i=0;i<50;i++){
            User user = new User("userName"+i,"userLstName"+i,i+"@mail.com",null);
            //user.setSession(session);
            userRepository.save(user);
            for (j=0;j<20;j++){
                int index = new Random().nextInt(4);
                Workout workout = new Workout(workouts[index], "desc",new Random().nextInt(8), new Random().nextInt(8), null, user);
                workoutRepository.save(workout);
            }
            userRepository.save(user);
        }
    }
}
