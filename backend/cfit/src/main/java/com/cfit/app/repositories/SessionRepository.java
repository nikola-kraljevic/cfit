package com.cfit.app.repositories;

import com.cfit.app.models.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {

    List<Session> findAllByDateBefore(Date date);

    List<Session> findAllByDateAfter(Date date);
}
