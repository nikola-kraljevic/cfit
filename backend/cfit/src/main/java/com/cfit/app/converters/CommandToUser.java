package com.cfit.app.converters;

import com.cfit.app.commands.UserCommand;
import com.cfit.app.models.User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class CommandToUser implements Converter<UserCommand, User> {

    private Date parseDate(String date, String format) throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date);
    }

    @Synchronized
    @Nullable
    @Override
    public User convert(UserCommand source) {

        if (source == null) {
            return null;
        }

        final User user = new User();
        user.setFirstName(source.getFirstName());
        user.setLastName(source.getLastName());
        user.setEmail(source.getEmail());
        user.setDiagnostics(source.getDiagnostics());
        try {
            user.setSubscription(parseDate(source.getSubscription(),"yyyy-DD-mm"));//string is YYYY-DD-MM
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return user;
    }
}
