package com.cfit.app.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
public class SessionCommand {

    private Long id;
    private Date date;
    private Integer numOfAllowedUsers;
}
