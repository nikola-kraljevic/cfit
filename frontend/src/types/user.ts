import {SessionDto} from "./session";

export type UserDto = {
    id: number,
    firstName:string,
    lastName:string,
    email:string,
    diagnostics:string,
    subscription:number
    sessions: SessionDto[];
}
