import {UserDto} from "./user";

export type SessionDto = {
    id: number,
    date: number,
    numOfAllowedUsers: number
    users: UserDto[]
}