export type WorkoutDto = {
    id: number,
    name: string,
    reps: number,
    sets: number,
    kgs: number,
    description: string,
    date: number
}