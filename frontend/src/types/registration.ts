export type RegistrationDto = {
    firstName: string,
    lastName: string,
    email: string
}