import React, { useState, useEffect } from 'react'
import axios from 'axios';
import {Field, Form, Formik, FormikActions} from 'formik'
import {UserDto} from "../../../types/user";
import {WorkoutDto} from "../../../types/workout"
import Table from "reactstrap/lib/Table";
import {Container, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input} from "reactstrap";
import {navigate} from 'hookrouter'
import { format } from 'date-fns'


type Props = {id: Number}

//todo diagnostic results & pic
//todo error: on new user registration Update user form doesnot show

const EditUser: React.FC<Props> = (props) =>{

    const [diag, setDiag] = useState('')
    const [image ,setImage] = useState()
    const [toggleWarn, setToggleWarn] = useState(false)
    const [toggle, setToggle] = useState(false)
    const [user, setUser] = useState<UserDto | null>(null)
    const [workout, setWorkout] = useState<null | WorkoutDto>(null)
    const [workouts, setWorkouts] = useState<null | [WorkoutDto]>(null)
    const [error, setError] = useState()

    function handleToggle(){
        setToggle(!toggle)
    }

    function handleToggleWarn(){
        setToggleWarn(!toggleWarn)
    }

    function getWorkouts(){
        axios.get(process.env.REACT_APP_API_URL+'/api/workout/' + props.id)
            .then((response) => {
                setWorkouts(response.data)
            })
    }

    function getUser(){
        axios.get(process.env.REACT_APP_API_URL+'/api/user/'+props.id)
            .then((response) => {
                console.log('Get succes')
                setUser(response.data)
            })
    }

    function handleUserUpdate(val, bag: FormikActions<UserDto>) {
        axios.post(process.env.REACT_APP_API_URL+'/api/user/update',val)
            .then((response) => {
                console.log(val);
                bag.setSubmitting(false)
            })
    }

    function handleWorkoutAdd(val, bag:FormikActions<null | WorkoutDto>) {
        axios.post(process.env.REACT_APP_API_URL+'/api/user/workout/' + props.id, val)
            .then((response) =>{
                console.log('succes' + response);
                bag.setSubmitting(false)
                setWorkout(null)
                getWorkouts()
            })
    }

    function handleDeleteWorkout(id) {
        axios.post(process.env.REACT_APP_API_URL+'/api/workout/', {userId:id})
            .then((response) => {
                console.log('deleted', id)
                getWorkouts()
            }).catch((err) => {
                console.log(id + "  " +  err)
        })
    }

    function handleDeleteUser(){
        axios.delete(process.env.REACT_APP_API_URL+'/api/user/delete/' + props.id)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
            })
        navigate('/admin')
    }

    function handleWarnUser(){
        if(user == null){return}
        axios.post(process.env.REACT_APP_API_URL+'/email/warn', {email:user.email, firstName:user.firstName})
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
            })
        setToggleWarn(false)
    }

    function handleImageUpload(val){
        console.log(val)
        axios.post(process.env.REACT_APP_API_URL+'/api/user/image/' + props.id, val)
            .then((response) => {
                console.log(response);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    function handleDiagChange(e) {
        setDiag(e.target.value)
    }

    function handleDiagnosticsUpdate(){
        console.log(diag)
        axios.post(process.env.REACT_APP_API_URL+'/api/user/diagnostics/' + props.id, {diag: diag})
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
            })
    }

    useEffect(() => {
        let isSubscribed = true
        axios.get(process.env.REACT_APP_API_URL+'/api/workout/' + props.id)
            .then((response) => isSubscribed? (setWorkouts(response.data)) : null)
            .catch((err) => isSubscribed? (setError(err)) : null)
        axios.get(process.env.REACT_APP_API_URL+'/api/user/'+props.id)
            .then((response) =>
                isSubscribed? (
                    setUser({...response.data, subscription: response.data.subscription && format(response.data.subscription, 'yyyy-MM-dd')}),
                    console.log(response.data),
                    setDiag(response.data.diagnostics)) : null)
            .catch((err) => isSubscribed? (setError(err)) : null)
        return () => {isSubscribed = false}
    },[])

    return(
        <>
            <Container>
                <Row>
                    <Col>
                        {user &&
                        <Formik
                        onSubmit={handleUserUpdate}
                        initialValues={user}
                        render={({errors, status, touched, isSubmitting}) =>
                                <Form>
                                    <Field type="text" name="firstName" placeholder="Fname"/>
                                    <Field type="text" name="lastName" placeholder="Lname"/>
                                    <Field type="email" name="email" placeholder="email"/>
                                    <Field type="date" name="subscription" placeholder="Subs"/>
                                    <Button type="submit" disabled={isSubmitting}>Submit</Button>
                                </Form>}
                        />}
                    </Col>
                    <Col>
                        <Formik
                        initialValues={workout}
                        onSubmit={handleWorkoutAdd}
                        render={({errors, status, touched, isSubmitting}) =>
                            <Form>
                                <Field type="text" name="name" placeholder="name"/>
                                <Field type="number" name="reps" placeholder="reps"/>
                                <Field type="number" name="sets" placeholder="sets"/>
                                <Field type="number" name="kgs" placeholder="kgs"/>
                                <Field type="textarea" name="description"  placeholder="desc"/>
                                <Button type="submit" disabled={isSubmitting}>Add Workout</Button>
                            </Form>
                        }
                        />
                    </Col>
                    <Col>
                        <Row><Button color="danger" onClick={handleToggle}>Delete</Button></Row>
                            <Modal isOpen={toggle} toggle={handleToggle}>
                                <ModalHeader toggle={handleToggle}>WARNING!</ModalHeader>
                                <ModalBody>
                                    Are you sure you want to delete user?
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="danger" onClick={handleDeleteUser}>Delete</Button>{' '}
                                    <Button color="primary" onClick={handleToggle}>Cancel</Button>
                                </ModalFooter>
                            </Modal>
                        <Row><Button color="warning" onClick={handleToggleWarn}>Warn</Button></Row>
                            <Modal isOpen={toggleWarn} toggle={handleToggleWarn}>
                                <ModalHeader toggle={handleToggleWarn}>WARNING!</ModalHeader>
                                <ModalBody>
                                    Are you sure you want to warn user?
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="warning" onClick={handleWarnUser}>Warn</Button>{' '}
                                    <Button color="primary" onClick={handleToggleWarn}>Cancel</Button>
                                </ModalFooter>
                            </Modal>
                        <Row>
                            <form encType="multipart/form-data">
                                <input id="file" name="file" type="file" onChange={(event) => {
                                    setImage(event.currentTarget.files && event.currentTarget.files[0]);
                                }} />
                                <Button color="success" onClick={handleImageUpload} disabled={true}>Upload Image</Button>
                            </form>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className="kontenjer">
                            <Table>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>name</th>
                                        <th>kgs</th>
                                        <th>reps</th>
                                        <th>sets</th>
                                        <th>date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {workouts && workouts.map((workout) =>
                                    <tr key={workout.id}>
                                        <td><Button color="danger" value={workout.id} onClick={() => handleDeleteWorkout(workout.id)}>X</Button></td>
                                        <td>{workout.name}</td>
                                        <td>{workout.kgs}</td>
                                        <td>{workout.reps}</td>
                                        <td>{workout.sets}</td>
                                        <td>{format(workout.date, 'dd.MM')}</td>
                                    </tr>
                                )}
                                </tbody>
                            </Table>
                        </div>
                    </Col>
                    <Col>
                        <Input rows={10} type="textarea" name="text" id="exampleText" onChange={handleDiagChange} value={diag}/>
                        <Button onClick={handleDiagnosticsUpdate} color="success">Update</Button>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default EditUser;