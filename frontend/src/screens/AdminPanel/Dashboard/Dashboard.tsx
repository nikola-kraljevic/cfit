import React from 'react';
import {Button, Col, Container, Row} from 'reactstrap';
import {navigate} from 'hookrouter';
import Sessions from "../Sessions/Sessions";
import Users from '../Users/Users'
import MailEveryone from "../MailEveryone/MailEveryone";

const AdminPanelComponent:React.FC = () => {

    function handleRegisterClick(){
        navigate('/admin/register')
    }

    return(
        <div className="admin-wrapper">
            <div className="adminOne">
                <div className="adminContent">
                    <Button color="success" onClick={handleRegisterClick}>Register User</Button>
                    <Users/>
                </div>
            </div>
            <div className="adminTwo">
                <div className="adminContent">
                    <Sessions/>
                </div>
            </div>
            <div className="adminThree">
                <div className="adminContent">
                    <MailEveryone/>
                </div>
            </div>
        </div>
    )
}

export default AdminPanelComponent;