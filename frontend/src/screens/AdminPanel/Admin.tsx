import React, {useContext} from 'react'
import AdminPanelComponent from './Dashboard/Dashboard';
import Register from './Register/Register';
import ErrorComponent from '../../components/ErrorComponent'
import Edit from './Edit/EditUser'
import {useRoutes} from 'hookrouter';
import AuthCtx from "../../contexts/AuthCtx";
import LoggedInError from "../../components/LoggedInError";

const Admin:React.FC = () => {

    const authCtx = useContext(AuthCtx)

    const routes = {
        '': () => <AdminPanelComponent />,
        '/': () => <AdminPanelComponent />,
        '/register': () => <Register />,
        '/edit/:id': ({id}) => <Edit id={id}/>
    }

    const routeResult = useRoutes(routes);
    
    return(
        authCtx.authenticated? (
        <>
            { routeResult }
        </>
        ) : (
            <LoggedInError/>
        )
    );
}

export default Admin;