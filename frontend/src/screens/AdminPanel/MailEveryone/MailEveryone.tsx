import React, {useState} from 'react'
import {Container, Input, Form, Button} from "reactstrap";
import axios from 'axios'

const MailEveryone:React.FC = () => {

    const [subject, setSubject] = useState('');
    const [text, setText] = useState('');
    const [disabled, setDisabled] = useState(false);

    function handleSubjectChange(e) {
        setSubject(e.target.value)
    }
    function handleTextChange(e) {
        setText(e.target.value)
    }

    function handleSendMail() {
        setDisabled(true);
        axios.post(process.env.REACT_APP_API_URL + '/email/everyone', {subject:subject, text:text})
            .then((response) => {
                setDisabled(false);
                console.log(response);
            })
            .catch((err) => {
                setDisabled(false);
                console.log(err);
            })
    }

    return(
        <Container>
            <Form>
                <Input type="text" name="subject" id="subject" placeholder="Subject" onChange={handleSubjectChange}/>
                <Input type="textarea" name="text" id="text" rows={10} onChange={handleTextChange} />
                <Button onClick={handleSendMail} disabled={disabled}>Send</Button>
            </Form>
        </Container>
    );
}

export default MailEveryone;