import React, {useContext, useEffect, useState} from 'react'
import {Table} from 'reactstrap'
import axios from 'axios'
import {navigate} from 'hookrouter'
import AppCtx from "../../../contexts/AppCtx";
import {WorkoutDto} from "../../../types/workout";
import {format} from "date-fns";

const Workouts = () =>{

    const [workouts, setWorkouts] = useState<[WorkoutDto]>()

    const ctx = useContext(AppCtx)

    useEffect(() => {
        if(ctx.user == null){
            return
        }
        let isSubscribed = true
        console.log('Workouts effect')
        axios.get(process.env.REACT_APP_API_URL+'/api/workout/' + ctx.user.id)
            .then((response) => isSubscribed? (setWorkouts(response.data)):null)
        return () => {isSubscribed = false}
    },[ctx.user])

    return(
        <div className="kontenjer">
            <Table borderless dark style={{backgroundColor:"rgba(52,58,64, 0.4)"}}>
            <thead>
            <tr>
                <th>Naziv vježbe</th>
                <th>Težina</th>
                <th>Ponavljanja</th>
                <th>Serija</th>
                <th>Datum</th>
            </tr>
            </thead>
                <tbody>
                {workouts && workouts.map((workout) =>
                    <tr key={workout.id}>
                        <td>{workout.name}</td>
                        <td>{workout.kgs}</td>
                        <td>{workout.reps}</td>
                        <td>{workout.sets}</td>
                        <td>{format(workout.date, 'dd.MM.yyyy')}</td>
                    </tr>
                )}
                </tbody>
            </Table>
        </div>
    );
}

export default Workouts;