import React, {useContext, useEffect} from 'react'
import AppCtx from "../../contexts/AppCtx";
import {Button, Col, Container, Jumbotron, Row} from 'reactstrap'
import Workouts from "./Workouts/Workouts";
import Sessions from "./Sessions/Sessions";
import axios from 'axios'
import LoggedInError from "../../components/LoggedInError";
import {format} from "date-fns";
import backgroundpiktir from "../../images/backgroundpiktir.jpg"
import {updateLocale} from "moment";

const User:React.FC = () =>{

    const ctx = useContext(AppCtx)

    useEffect(() => {
        if (ctx.user == null){return;}
        let isSubscribed = true
        console.log('User effect')
        axios.get(process.env.REACT_APP_API_URL+'/api/user/' + ctx.user.id)
            .then((response) => isSubscribed?
                (ctx.updateUser({...response.data}),
                localStorage.setItem('user', JSON.stringify({...response.data}))) : null)
        return() =>{isSubscribed = false}
    },[])

    return(
        ctx.user?(
            <div className="dashboardBackground">
                <Container>
                    <Row>
                        <Col sm="5">
                            <Workouts/>
                        </Col>
                        <Col sm="1"></Col>
                        <Col sm="5">
                            { ctx.user && <Sessions/>}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                        {ctx.user && ctx.user.diagnostics &&
                            <div className="info diag">
                                <h2 style={{color:"white"}}>Dijagnostika</h2>
                                <p className="lead"
                                   style={{whiteSpace: "pre-wrap", fontSize:"20px", fontWeight:"bold"}}>
                                    {ctx.user.diagnostics}
                                </p>
                            </div>}
                        </Col>
                    </Row>
                </Container>
            </div>
        ) : (
            <LoggedInError/>
        )

    );
}

export default User;