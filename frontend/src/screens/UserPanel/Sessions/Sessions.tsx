import React, {useContext, useEffect, useState} from 'react'
import {SessionDto} from "../../../types/session";
import axios from "axios";
import {Button, Spinner, Table} from "reactstrap";
import {navigate} from 'hookrouter'
import AppCtx from "../../../contexts/AppCtx";
import { format } from 'date-fns'


const Sessions: React.FC = () => {

    const [sessions, setSessions] = useState<SessionDto[]>([])
    const ctx = useContext(AppCtx)

    function handleRegisterClick(sessionId,userId){
        axios.post(process.env.REACT_APP_API_URL+'/api/session/register',{sessionId: sessionId, userId: userId})
            .then((response) => {
                console.log(response)
                ctx.updateUser({...ctx.user, sessions: response.data})
                localStorage.setItem('user',JSON.stringify({...ctx.user, sessions: response.data}))
            })
            .catch((err) => {
                console.log(err)
                navigate('/err')
            })
    }

    function handleCancelClick(sessionId,userId){
        axios.post(process.env.REACT_APP_API_URL+'/api/user/cancel',{userId,sessionId})
            .then((response) => {
                console.log(response)
                ctx.updateUser(response.data)
                localStorage.setItem('user',JSON.stringify(response.data))
            })
            .catch((error) => {
                console.log(error)
            })

    }

    useEffect(() => {
        let isSubscribed = true;
        console.log('Sessions effect')
        axios.post(process.env.REACT_APP_API_URL+'/api/session/available', {userId:ctx.user && ctx.user.id, sessionId:null})
            .then((response) => isSubscribed? (setSessions(response.data)) : null)
            .catch((err) => isSubscribed? (console.log(err)) : (null))
        return () => {isSubscribed = false}
    },[ctx.user])

    return(
        <div>
            {ctx.user &&
            <>
                <Table dark borderless style={{backgroundColor:"rgba(52,58,64, 0.4)"}}>
                    <thead>
                    <tr>
                        <th>
                            Termini
                        </th>
                        <th>Br. mjesta</th>
                    </tr>
                    </thead>
                    {sessions ?(
                    sessions.map((session) =>
                        <tbody>
                            <tr key={session.id}>
                                <td>{format(session.date, 'dd.MM HH:mm')}</td>
                                <td>{session.numOfAllowedUsers}</td>
                                <td>
                                    {ctx.user &&
                                    <Button color="success" onClick={() => handleRegisterClick(session.id,ctx.user && ctx.user.id)}>Prijava</Button>
                                    }
                                </td>
                            </tr>
                        </tbody>
                    )) : (
                            <Spinner color="primary" />
                        )}
                </Table>
                {(ctx.user && ctx.user.sessions && ctx.user.sessions.length>0)?
                    (<Table dark borderless style={{backgroundColor:"rgba(52,58,64, 0.4)"}}>
                        <thead>
                        <tr>
                            <th>
                                Termini
                            </th>
                            <th>Br. mjesta</th>
                        </tr>
                        </thead>
                        {ctx.user.sessions ?(
                            ctx.user.sessions.map((session) =>
                                <tbody>
                                <tr key={session.id}>
                                    <td>{format(session.date, 'dd.MM HH:mm')}</td>
                                    <td>{session.numOfAllowedUsers}</td>
                                    <td>
                                        {ctx.user &&
                                        <Button color="danger" onClick={() => handleCancelClick(session.id,ctx.user && ctx.user.id)}>Otkaži</Button>
                                        }
                                    </td>
                                </tr>
                                </tbody>
                            )) : (
                            <Spinner color="primary" />
                        )}
                    </Table>) : (<></>)}
            </>
            }
        </div>
    )
}

export default Sessions