import React, {useCallback, useContext, useState} from 'react';
import {Container, Col, Form, FormGroup, Input, Button, Alert,} from 'reactstrap';
import axios from 'axios'
import {navigate} from 'hookrouter'
import './style/LoginFormStyle.css';
import {UserDto} from "../types/user";
import AppCtx from "../contexts/AppCtx";

type Props = {
    onUserLogin(user: UserDto): void
}


const LoginForm:React.FC<Props> = (props) => {
    
    const [email, setEmail]= useState('')
    const [password, setPassword]= useState('')
    const [errorMessage, setErrorMessage] = useState(false)

    function handleUserNameChange(e){
        setEmail(e.target.value);
    }
    
    function handlePasswordChange(e){
        setPassword(e.target.value);
    }

    const handleLogin = useCallback(() => {
        axios.post(process.env.REACT_APP_API_URL+'/api/user/login',{email, password})
            .then((response) => {
                setErrorMessage(false)
                props.onUserLogin(response.data)
                navigate('/dashboard')
            }).catch((err) => {
                console.log(err)
                setErrorMessage(true)
        })
    },[email,password])

    return(
    <>
    <div className="login">
        <Form className="form" style={{margin:"0 auto", paddingTop:"20px"}}>
            <Col style={{margin:"0 auto"}}>
                <FormGroup>
                <Input
                    value={email}
                    onChange={handleUserNameChange}
                    type="text"
                    name="userName"
                    id="exampleUsername"
                    placeholder="Email"
                />
                </FormGroup>
            </Col>
            <Col style={{margin:"0 auto"}}>
                <FormGroup>
                <Input
                    value={password}
                    onChange={handlePasswordChange}
                    type="password"
                    name="password"
                    id="examplePassword"
                    placeholder="********"
                />
                </FormGroup>
            </Col>
            <Button color="success" onClick = {handleLogin}>Prijavi se</Button>
            {errorMessage && <Alert color="danger">Prijava nije uspjela, pokušajte opet</Alert>}
        </Form>
    </div>
    </>
    );
}

export default LoginForm;