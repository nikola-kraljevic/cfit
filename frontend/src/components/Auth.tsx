import React, {useCallback, useContext, useState} from 'react'
import {Alert, Button, Col, Container, Form, FormGroup, Input} from "reactstrap";
import axios from "axios";
import {navigate} from 'hookrouter'
import AuthCtx from "../contexts/AuthCtx";

const Auth:React.FC = () =>{

    const authContext = useContext(AuthCtx)

    const[username, setUsername] = useState('')
    const[password, setPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState(false)

    function handleUserNameChange(e){
        setUsername(e.target.value);
    }

    function handlePasswordChange(e){
        setPassword(e.target.value);
    }

    console.log("authContext function val:" + authContext.updateContext)

    const handleLogin = useCallback(() => {
        axios.post(process.env.REACT_APP_API_URL+'/api/user/auth',{email: username,password: password})
            .then((response) => {
                if (response.data) {
                    console.log( process.env.REACT_APP_API_URL+"/api/user/auth response " + response)
                    setErrorMessage(false)
                    authContext.updateContext!()
                } else {
                    setErrorMessage(true)
                }
            })
            .catch((error) => {
                console.log(error)
                setErrorMessage(true)
            })
    }, [authContext.updateContext,username,password])

    return(
        <>
        <Container className="Login">
            <Form className="form">
                <Col>
                    <FormGroup>
                        <Input
                            value={username}
                            onChange={handleUserNameChange}
                            type="text"
                            name="userName"
                            id="exampleUsername"
                            placeholder="User Name"
                        />
                    </FormGroup>
                </Col>
                <Col>
                    <FormGroup>
                        <Input
                            value={password}
                            onChange={handlePasswordChange}
                            type="password"
                            name="password"
                            id="examplePassword"
                            placeholder="********"
                        />
                    </FormGroup>
                </Col>
                <Button color="success" onClick={handleLogin}>Log in</Button>
                {errorMessage && <Alert color="danger">Login failed, please try again</Alert>}
            </Form>
        </Container>
        </>
    );
}

export default Auth