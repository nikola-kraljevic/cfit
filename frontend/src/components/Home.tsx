import * as React from 'react';
import {IoLogoFacebook, IoLogoInstagram, IoIosPin} from "react-icons/all";

const Marker = ({ lat, lng, text }) => (
    <div style={{
        color: 'white',
        background: 'grey',
        padding: '8px 8px',
        display: 'inline-flex',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '100%',
        transform: 'translate(-50%, -50%)'
    }}>
        {text}
    </div>
);

const Home:React.FC = () =>{
    return(
        <div className="wrapper">
            <div className="info one">
            </div>
            <div className="info two">
            </div>
            <div className="info three">
            </div>
            <div className="info four">
            </div>
            <div className="info footer">
                <div className="kontentOne">
                    <p>
                        <a style={{color:"white"}} href="https://www.instagram.com/improve.sps/?igshid=atmq70ab291d"><IoLogoInstagram size="1.25em" /></a>
                        <a style={{color:"white"}} href="https://www.facebook.com/Improve-126031172128674/"><IoLogoFacebook size="1.25em" /></a>
                        <a style={{fontSize:"13px"}}><IoIosPin style={{color:"white"}} size="1.6em"/>Ozaljska 37, 10000 Zagreb</a>
                    </p>
                </div>
            </div>
        </div>
    );
}

export default Home