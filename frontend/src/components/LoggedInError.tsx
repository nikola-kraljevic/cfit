import React from 'react'
import {Container, Jumbotron} from "reactstrap";

const LoggedInError:React.FC = () => {
    return(
        <>
            <Jumbotron fluid>
                <Container fluid>
                    <h1 className="display-3">Error</h1>
                    <p className="lead">You need to login first</p>
                </Container>
            </Jumbotron>
        </>
    );
}

export default LoggedInError